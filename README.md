n3t Virtuemart Simple
=====================

n3t Virtuemart Simple Joomla! plugin allows to simplify Virtuemart administration, hide unused fields.

Installation
------------

n3t Virtuemart Simple is Joomla! system plugin. It could be installed as any other extension in
Joomla!

After installing **do not forget to enable the plugin** from Plugin Manager in your
Joomla! installation.

Configuration
-------------

Go to plugin settings and check which fields / buttons etc. should not be visible in administration
of product / category and other pages.
