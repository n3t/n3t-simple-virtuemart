<?php
/**
 * @package n3t Simple Virtuemart
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2020-2021 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;

class plgSystemN3tVmSimple extends CMSPlugin
{

  private function isNewTemplate(): bool
  {
    if (!class_exists( 'VmConfig' )) {
      require(JPATH_ROOT .'/administrator/components/com_virtuemart/helpers/config.php');
      VmConfig::loadConfig();
    }

    return !!VmConfig::get('backendTemplate',0);
  }

  private function addScript(string $script): void
  {
    if ($script) {
      $doc = Factory::getDocument();
      HTMLHelper::_('jquery.framework');
      $doc->addScriptDeclaration("jQuery(function() {" . $script . "});");
    }
  }

  private function addStyleSheet(string $style): void
  {
    if ($style) {
      $doc = Factory::getDocument();
      $doc->addStyleDeclaration($style);
    }
  }

  private function handleProduct(): void
  {
    $script = '';
    $style = '';

    if ($this->isNewTemplate()) {
      if ($this->params->get('product_hide_discontinued'))
        $script .= "jQuery('input[name=product_discontinued]').closest('.uk-clearfix').css('display', 'none');";

      if ($this->params->get('product_hide_special'))
        $script .= "jQuery('input[name=product_special]').closest('.uk-clearfix').css('display', 'none');";

      if ($this->params->get('product_hide_sku')) {
        $script .= "jQuery('input[name=product_sku]').closest('.uk-clearfix').css('display', 'none');";
      }

      if ($this->params->get('product_hide_gtin')) {
        $script .= "jQuery('input[name=product_gtin]').closest('.uk-clearfix').css('display', 'none');";
      }

      if ($this->params->get('product_hide_mpn')) {
        $script .= "jQuery('input[name=product_mpn]').closest('.uk-clearfix').css('display', 'none');";
      }

      if ($this->params->get('product_hide_layout')) {
        $script .= "jQuery('select[name=layout]').closest('.uk-clearfix').css('display', 'none');";
      }

      if ($this->params->get('product_hide_url')) {
        $script .= "jQuery('input[name=product_url]').closest('.uk-clearfix').css('display', 'none');";
      }

      if ($this->params->get('product_hide_shoppergroup')) {
        $script .= "jQuery('select[name*=virtuemart_shoppergroup_id]').closest('td').css('display', 'none');";
        $script .= "jQuery('select[name*=virtuemart_shoppergroup_id]').closest('.uk-clearfix').css('display', 'none');";
      }

      if ($this->params->get('product_hide_manufacturer')) {
        $script .= "jQuery('select[name*=virtuemart_manufacturer_id]').closest('.uk-clearfix').css('display', 'none');";
      }

      if ($this->params->get('product_hide_canon_category')) {
        $script .= "jQuery('select[name*=product_canon_category_id]').closest('.uk-clearfix').css('display', 'none');";
      }

      if ($this->params->get('product_price_always_final')) {
        $script .= "jQuery('input[name*=product_price]').prop('readonly', true);";
        $script .= "jQuery('input[name*=product_price]').closest('.uk-clearfix').css('display', 'none');";

        $script .= "jQuery('input[name*=use_desired_price]').css('display', 'none').prop('checked', true);";
        $script .= "jQuery('input[name*=use_desired_price]').next('strong').css('display', 'none');";
      } elseif ($this->params->get('product_price_hide_final')) {
        $script .= "jQuery('input[name*=salesPrice]').closest('.uk-clearfix').css('display', 'none');";

        $script .= "jQuery('input[name*=use_desired_price]').css('display', 'none').prop('checked', false);";
        $script .= "jQuery('input[name*=use_desired_price]').next('strong').css('display', 'none');";
      } elseif ($this->params->get('product_price_final_readonly')) {
        $script .= "jQuery('input[name*=salesPrice]').prop('readonly', true);";

        $script .= "jQuery('input[name*=use_desired_price]').css('display', 'none').prop('checked', false);";
        $script .= "jQuery('input[name*=use_desired_price]').next('strong').css('display', 'none');";
      }

      if ($this->params->get('product_price_always_final') && $this->params->get('product_price_hide_override')) {
        $script .= "jQuery('input[name*=product_override_price]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name*=product_override_price]').closest('td').prev('td').css('display', 'none');";
        $script .= "jQuery('input[name*=product_override_price]').closest('td').next('td').css('display', 'none');";
      } elseif ($this->params->get('product_price_hide_override')) {
        $script .= "jQuery('input[name*=product_override_price]').closest('div').css('display', 'none');";
        $script .= "jQuery('input[name*=product_override_price]').closest('td').find('.controls').css('display', 'none');";
        $script .= "jQuery('input[name*=product_override_price]').closest('td').prev('td').css('display', 'none');";
        $script .= "jQuery('input[name*=product_override_price]').closest('td').next('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_price_publish')) {
        $script .= "jQuery('input[name*=product_price_publish_up]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name*=product_price_publish_down]').closest('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_price_quantity')) {
        $script .= "jQuery('input[name*=price_quantity_start]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name*=price_quantity_end]').closest('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_base_price')) {
        $script .= "jQuery('input[name*=basePrice]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name*=basePrice]').closest('td').prev('td').html(' ');";
      }

      if ($this->params->get('product_hide_tax')) {
        $script .= "jQuery('input[name*=basePrice]').closest('td').next('td').css('display', 'none');";
        $script .= "jQuery('input[name*=basePrice]').closest('td').next('td').next('td').css('display', 'none');";
        $script .= "jQuery('input[name*=basePrice]').closest('td').next('td').next('td').next('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_currency')) {
        $script .= "jQuery('select[name*=product_currency]').closest('td').css('display', 'none');";
        $script .= "jQuery('select[name*=product_currency]').closest('td').next('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_discount')) {
        $script .= "jQuery('select[name*=product_discount_id]').closest('td').css('display', 'none');";
        $script .= "jQuery('select[name*=product_discount_id]').closest('td').next('td').css('display', 'none');";
      }

      $script .= "jQuery('.productPriceTable td:nth-child(5)').css('display', 'none');";

      if ($this->params->get('product_hide_multiple_price'))
        $script .= "jQuery('#add_new_price, .price_ordering, .price-remove').css('display', 'none');";


      if ($this->params->get('product_hide_variant')) {
        $script .= "jQuery('#product_parent_id').closest('.uk-card').parent().css('display', 'none');";
      }

      if ($this->params->get('product_hide_intnotes'))
        $script .= "jQuery('textarea[name*=intnotes]').closest('.uk-card').parent().css('display', 'none');";

      if ($this->params->get('product_hide_meta'))
        $script .= "jQuery('#customtitle').closest('.uk-card').parent().css('display', 'none');";

      if ($this->params->get('product_hide_status'))
        $script .= "jQuery('input[name=product_in_stock]').closest('.uk-card').parent().css('display', 'none');";

      if ($this->params->get('product_hide_mailing'))
        $script .= "jQuery('#mail-subject').closest('.uk-card').parent().css('display', 'none');";

      if ($this->params->get('product_hide_status') && $this->params->get('product_hide_mailing'))
        $script .= "jQuery('.vmuikit-admin-tabs li:nth-child(3)').css('display', 'none');";

      if ($this->params->get('product_hide_dims'))
        $script .= "jQuery('.vmuikit-admin-tabs li:nth-child(4)').css('display', 'none');";

      if ($this->params->get('product_hide_related'))
        $script .= "jQuery('#relatedproductsSearch').closest('.uk-card').parent().css('display', 'none');";

      if ($this->params->get('product_hide_related_categories'))
        $script .= "jQuery('#relatedcategoriesSearch').closest('.uk-card').parent().css('display', 'none');";

      if ($this->params->get('product_hide_image_details'))
        $script .= "jQuery('input[name*=media_published]').closest('.uk-grid').css('display', 'none');";
    } else {
      if ($this->params->get('product_hide_discontinued'))
        $script .= "jQuery('input[name=product_discontinued]').closest('label').css('display', 'none');";

      if ($this->params->get('product_hide_special'))
        $script .= "jQuery('input[name=product_special]').closest('label').css('display', 'none');";

      if ($this->params->get('product_hide_sku')) {
        $script .= "jQuery('input[name=product_sku]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name=product_sku]').closest('td').prev('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_gtin')) {
        $script .= "jQuery('input[name=product_gtin]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name=product_gtin]').closest('td').prev('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_mpn')) {
        $script .= "jQuery('input[name=product_mpn]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name=product_mpn]').closest('td').prev('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_layout')) {
        $script .= "jQuery('select[name=layout]').closest('td').css('display', 'none');";
        $script .= "jQuery('select[name=layout]').closest('td').prev('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_url')) {
        $script .= "jQuery('input[name=product_url]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name=product_url]').closest('td').prev('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_shoppergroup')) {
        $script .= "jQuery('select[name*=virtuemart_shoppergroup_id]').closest('td').css('display', 'none');";
        $script .= "jQuery('select[name*=virtuemart_shoppergroup_id]').closest('td').prev('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_manufacturer')) {
        $script .= "jQuery('select[name*=virtuemart_manufacturer_id]').closest('td').css('display', 'none');";
        $script .= "jQuery('select[name*=virtuemart_manufacturer_id]').closest('td').prev('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_canon_category')) {
        $script .= "jQuery('select[name*=product_canon_category_id]').closest('td').css('display', 'none');";
        $script .= "jQuery('select[name*=product_canon_category_id]').closest('td').prev('td').css('display', 'none');";
      }

      if ($this->params->get('product_price_always_final')) {
        $script .= "jQuery('input[name*=product_price]').prop('readonly', true);";
        $script .= "jQuery('input[name*=product_price]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name*=product_price]').closest('td').prev('td').css('display', 'none');";

        $script .= "jQuery('input[name*=use_desired_price]').css('display', 'none').prop('checked', true);";
        $script .= "jQuery('input[name*=use_desired_price]').next('strong').css('display', 'none');";
      } elseif ($this->params->get('product_price_hide_final')) {
        $script .= "jQuery('input[name*=salesPrice]').closest('td').css('visibility', 'hidden');";
        $script .= "jQuery('input[name*=salesPrice]').closest('td').prev('td').css('visibility', 'hidden');";

        $script .= "jQuery('input[name*=use_desired_price]').css('display', 'none').prop('checked', false);";
        $script .= "jQuery('input[name*=use_desired_price]').next('strong').css('display', 'none');";
      } elseif ($this->params->get('product_price_final_readonly')) {
        $script .= "jQuery('input[name*=salesPrice]').prop('readonly', true);";

        $script .= "jQuery('input[name*=use_desired_price]').css('display', 'none').prop('checked', false);";
        $script .= "jQuery('input[name*=use_desired_price]').next('strong').css('display', 'none');";
      }

      if ($this->params->get('product_price_always_final') && $this->params->get('product_price_hide_override')) {
        $script .= "jQuery('input[name*=product_override_price]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name*=product_override_price]').closest('td').prev('td').css('display', 'none');";
        $script .= "jQuery('input[name*=product_override_price]').closest('td').next('td').css('display', 'none');";
      } elseif ($this->params->get('product_price_hide_override')) {
        $script .= "jQuery('input[name*=product_override_price]').closest('div').css('display', 'none');";
        $script .= "jQuery('input[name*=product_override_price]').closest('td').find('.controls').css('display', 'none');";
        $script .= "jQuery('input[name*=product_override_price]').closest('td').prev('td').css('display', 'none');";
        $script .= "jQuery('input[name*=product_override_price]').closest('td').next('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_price_publish')) {
        $script .= "jQuery('input[name*=product_price_publish_up]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name*=product_price_publish_down]').closest('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_price_quantity')) {
        $script .= "jQuery('input[name*=price_quantity_start]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name*=price_quantity_end]').closest('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_base_price')) {
        $script .= "jQuery('input[name*=basePrice]').closest('td').css('display', 'none');";
        $script .= "jQuery('input[name*=basePrice]').closest('td').prev('td').html(' ');";
      }

      if ($this->params->get('product_hide_tax')) {
        $script .= "jQuery('select[name*=product_tax_id]').closest('td').css('display', 'none');";
        $script .= "jQuery('select[name*=product_tax_id]').closest('td').next('td').css('display', 'none');";
        $script .= "jQuery('select[name*=product_tax_id]').closest('td').next('td').next('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_currency'))
        $script .= "jQuery('select[name*=product_currency]').closest('td').css('display', 'none');";

      if ($this->params->get('product_hide_discount')) {
        $script .= "jQuery('select[name*=product_discount_id]').closest('td').css('display', 'none');";
        $script .= "jQuery('select[name*=product_discount_id]').closest('td').next('td').css('display', 'none');";
      }

      if ($this->params->get('product_hide_multiple_price'))
        $script .= "jQuery('#add_new_price, .price_ordering, .price-remove').css('display', 'none');";

      if ($this->params->get('product_hide_variant')) {
        $script .= "jQuery('#product_parent_id').prev('span').prev('.btn-wrapper').css('display', 'none');";
        $script .= "jQuery('#product_parent_id').prev('span').css('display', 'none');";
        $script .= "jQuery('#product_parent_id').css('display', 'none');";
      }

      if ($this->params->get('product_hide_intnotes'))
        $script .= "jQuery('textarea[name*=intnotes]').closest('fieldset').css('display', 'none');";

      if ($this->params->get('product_hide_meta'))
        $script .= "jQuery('#customtitle').closest('fieldset').css('display', 'none');";

      if ($this->params->get('product_hide_status'))
        $script .= "jQuery('input[name=product_in_stock]').closest('fieldset').css('display', 'none');";

      if ($this->params->get('product_hide_mailing'))
        $script .= "jQuery('#mail-subject').closest('fieldset').css('display', 'none');";

      if ($this->params->get('product_hide_status') && $this->params->get('product_hide_mailing'))
        $script .= "jQuery('input[name=product_in_stock]').closest('.tabs').remove();";

      if ($this->params->get('product_hide_dims'))
        $script .= "jQuery('input[name=product_length]').closest('.tabs').remove();";

      if ($this->params->get('product_hide_related'))
        $script .= "jQuery('.vmjs-relatedproductsSearch').closest('fieldset').css('display', 'none');";

      if ($this->params->get('product_hide_related_categories'))
        $script .= "jQuery('#relatedcategoriesSearch').closest('fieldset').css('display', 'none');";

      if ($this->params->get('product_hide_image_details'))
        $script .= "jQuery('.selectimage table').css('display', 'none');";

      $script .= "jQuery('.productPriceTable td:empty').css('display', 'none');";
      $script .= "jQuery('.productPriceTable span.hasTip br').css('display', 'none');";
      $script .= "jQuery('.productPriceTable').css('width', '100%');";
      $script .= "jQuery('.productPriceTable').css('white-space', 'nowrap');";
    }

    if ($this->params->get('product_hide_toolbar_child'))
      $script .= "jQuery('#toolbar-new').css('display', 'none');";
    if ($this->params->get('product_hide_toolbar_clone'))
      $script .= "jQuery('#toolbar-copy').css('display', 'none');";
    if ($this->params->get('product_hide_toolbar_rating'))
      $script .= "jQuery('#toolbar-default').css('display', 'none');";
    if ($this->params->get('product_hide_toolbar_permissions'))
      $script .= "jQuery('#toolbar-lock').css('display', 'none');";

    $this->addScript($script);
    $this->addStyleSheet($style);
  }

  private function handleProductList(): void
  {
    $script = '';
    $style = '';

    $style .= 'body.com_virtuemart.view-product.task- td.order {width: 100px; display: grid; grid-template-columns: 2fr 1fr 1fr}';
    $style .= 'body.com_virtuemart.view-product.task- td.order input {grid-column: 1/4}';

    if ($this->params->get('products_hide_batch_categories'))
      $script .= "jQuery('[id=toolbar-new]:nth-of-type(1)').css('display', 'none');";

    if ($this->params->get('products_hide_batch_usergroups'))
      $script .= "jQuery('[id=toolbar-new]:nth-of-type(2)').css('display', 'none');";

    if ($this->params->get('products_hide_variant'))
      $script .= "jQuery('[id=toolbar-new]:nth-of-type(3)').css('display', 'none');";

    if ($this->params->get('products_hide_clone'))
      $script .= "jQuery('[id=toolbar-copy]').css('display', 'none');";

    if ($this->params->get('products_hide_clone_tree'))
      $script .= "jQuery('#toolbar-default').css('display', 'none');";

    if ($this->params->get('products_hide_reviews'))
      $script .= "jQuery('#toolbar-lock').css('display', 'none');";

    $this->addScript($script);
    $this->addStyleSheet($style);
  }

  private function handleCategory(string $task): void
  {
    $script = '';
    $style = '';

    if ($this->isNewTemplate()) {
      if ($task == 'default') {
        $style .= 'body.com_virtuemart.view-category.task- td.vm-order {width: 100px}';
        $style .= 'body.com_virtuemart.view-category.task-default td.vm-order {width: 100px}';
        $style .= 'body.com_virtuemart.view-category.task- td.vm-order input {width: 50px}';
        $style .= 'body.com_virtuemart.view-category.task-default td.vm-order input {width: 50px}';
        $style .= 'body.com_virtuemart.view-category.task- table.uk-table td:nth-child(3) {width: 50%}';
        $style .= 'body.com_virtuemart.view-category.task-default table.uk-table td:nth-child(3) {width: 50%}';
      } else if ($task == 'edit' || $task == 'add') {
        if ($this->params->get('category_hide_limit'))
          $script .= "jQuery('input[name=limit_list_step]').closest('.uk-clearfix').css('display', 'none');";
        if ($this->params->get('category_hide_pagination'))
          $script .= "jQuery('input[name=limit_list_initial]').closest('.uk-clearfix').css('display', 'none');";
        if ($this->params->get('category_hide_meta'))
          $script .= "jQuery('input[name=customtitle]').closest('.uk-card').parent().css('display', 'none');";
        if ($this->params->get('category_hide_shopfront'))
          $script .= "jQuery('select[name=show_store_desc]').closest('.uk-card').parent().css('display', 'none');";
        if ($this->params->get('category_hide_template'))
          $script .= "jQuery('select[name=categorytemplate]').closest('.uk-card').parent().css('display', 'none');";
        if ($this->params->get('category_hide_shopfront') && $this->params->get('category_hide_template')) {
          $script .= "jQuery('select[name=show_store_desc]').closest('.tabs').css('display', 'none');";
          $script .= "jQuery('.vmuikit-admin-tabs li:nth-child(2)').css('display', 'none');";
        }
        if ($this->params->get('category_hide_image_details'))
          $script .= "jQuery('input[name*=media_published]').closest('.uk-first-column').css('display', 'none');";
      }
    } else {
      if ($task == 'edit' || $task == 'add') {
        if ($this->params->get('category_hide_limit'))
          $script .= "jQuery('input[name=limit_list_step]').closest('tr').css('display', 'none');";
        if ($this->params->get('category_hide_pagination'))
          $script .= "jQuery('input[name=limit_list_initial]').closest('tr').css('display', 'none');";
        if ($this->params->get('category_hide_meta'))
          $script .= "jQuery('input[name=customtitle]').closest('fieldset').css('display', 'none');";
        if ($this->params->get('category_hide_shopfront'))
          $script .= "jQuery('select[name=show_store_desc]').closest('fieldset').css('display', 'none');";
        if ($this->params->get('category_hide_template'))
          $script .= "jQuery('select[name=categorytemplate]').closest('fieldset').css('display', 'none');";
        if ($this->params->get('category_hide_shopfront') && $this->params->get('category_hide_template'))
          $script .= "jQuery('select[name=show_store_desc]').closest('.tabs').remove();";
        if ($this->params->get('category_hide_image_details'))
          $script .= "jQuery('.selectimage table').css('display', 'none');";
      }
    }

    if ($task == 'add') {
      if ($this->params->get('category_default_published'))
        $script .= "jQuery('input[name=published]').prop('checked', true);";
    }

    $this->addScript($script);
    $this->addStyleSheet($style);
  }

	public function onAfterRoute()
	{
  	$app = Factory::getApplication();
    $input = $app->input;

    if ($app->isClient('administrator') && $input->getCmd('option') == 'com_virtuemart' && $input->getCmd('view') == 'product') {
      $task = $input->getCmd('task', 'display');
      if (!$task)
        $task = 'display';

      if ($task == 'display') {
        $this->handleProductList();
      } else if ($task == 'edit' || $task == 'add') {
        $this->handleProduct();
      }
    }

    if ($input->getCmd('option') == 'com_virtuemart' && $input->getCmd('view') == 'category') {
      $task = $input->getCmd('task', 'default');
      if (!$task)
        $task = 'default';
      $this->handleCategory($task);
    }

	}
}
